<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18/03/2018
 * Time: 10:52
 */

namespace Azizyus\DataTableWrapper;


use Illuminate\Support\Facades\Route;

class DataTableHelper
{


    public static function routes()
    {

        Route::get("datatable-onOrderChange",["as"=>"datatable.onOrderChange","uses"=>"DataTableController@orderChange"]);
        Route::get("datatable-onIsActiveChanged",["as"=>"datatable.onIsActiveChanged","uses"=>"DataTableController@isActiveChange"]);
        Route::get("datatable-test",["as"=>"dataTable.test","uses"=>"DataTableController@test"]);



    }


}