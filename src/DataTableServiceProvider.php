<?php

namespace Azizyus\DataTableWrapper;

use Illuminate\Support\ServiceProvider;

class DataTableServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {



        $this->loadViewsFrom(__DIR__."/html","DataTable");

        $this->publishes([


            __DIR__."/DataTableController.php" => "app/Http/Controllers/DataTableController.php"

        ],"datatable");


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
