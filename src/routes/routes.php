<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18/03/2018
 * Time: 10:27
 */




   Route::group(["middleware"=>config("datatable.middleware")],function (){


       Route::get("datatable-onOrderChange",["as"=>"datatable.onOrderChange","uses"=>"Azizyus\DataTableWrapper\DataTableController@orderChange"]);
       Route::get("datatable-onIsActiveChanged",["as"=>"datatable.onIsActiveChanged","uses"=>"Azizyus\DataTableWrapper\DataTableController@isActiveChange"]);

       Route::get("datatable-test",["as"=>"dataTable.test","uses"=>"Azizyus\DataTableWrapper\DataTableController@test"]);

   });


