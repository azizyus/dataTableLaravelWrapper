<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 10/03/2018
 * Time: 09:14
 */


namespace Azizyus\DataTableWrapper;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Illuminate\Support\Facades\View;
abstract class AbstractDataTable
{


    public $result;
    public $columns;
    public $model;
    public $filteredCount=0;
    public $search;
    public $searchColumns;
    public $request;
    public $routePrefix;
    public $totalCount;
    public $isCrud;
    public function __construct(Request $request=null,$hasSort=true,$isCrud=true)
    {



        $columns = $this->columns();
        $searchColumns = $this->searchColumns();
        $model = $this->model();

        $this->isCrud = $isCrud;
        $this->routePrefix = $this->routePrefix();


        #### CANT FIND SEARCH COLUMNS INSIDE OF COLUMNS ####

        // ya know "*" means all so no need to check it is exist or etc etc
        if(!in_array("*",$searchColumns))
        {
            foreach ($searchColumns as $searchColumnName)
            {

//                if(!in_array($searchColumnName,$columns)) throw new \Exception("CANT FIND SEARCH COLUMN -> $searchColumnName");

            }
        }

        #### CANT FIND SEARCH COLUMNS INSIDE OF COLUMNS ####

        if($request!=null)
        {




            $length= $request->length;
            $offset  = $request->start;
            $this->search  = $request->search["value"];
            $this->columns = $columns;
            $this->searchColumns = $searchColumns;

            $this->request = $request;



            if($hasSort) $this->model =   $model::orderBy("sort","ASC");
            else $this->model = $model::orderBy("id","DESC");

            $this->model = $this->model->select($columns);



            ##### DATATABLE STANDART SEARCH PROCESS ####

            if($this->search!=null && $this->search!="")
            {

                foreach ($searchColumns as $c)
                {

                  if($c!="*")  $this->model = $this->model->orWhere($c,"LIKE","%$this->search%");


                }

            }
            ##### DATATABLE STANDART SEARCH PROCESS ####

            ##### DATATABLE SEARCH IN RELATIONS #####
            if($this->search!=null)
            {

                $relationQueries = $this->relationQueries();
                foreach ($this->relationFilters() as $key => $filters)
                {

//               dd($this->search);
                    foreach ($filters as $column)
                    {
                        $this->model = $this->model->orWhereHas($key,function($query) use($relationQueries,$column) {

                            foreach ($relationQueries as $rKey => $r)
                            {
                                $query->where($rKey,$r);
                            }
                            $query->where($column,"LIKE","%$this->search%");

                        });

                    }
                }
            }
            ##### DATATABLE SEARCH IN RELATIONS #####




            $this->model = $this->extraQuery(); //RUN EXTRA QUERIES


            $this->model = $this->initRelations($this->model);


            //all count before filters
            $this->totalCount = $this->model->count();

            $this->filterRegister($request);


            //all counter after filters
            $this->filteredCount = $this->model->count();

            //classic pagination
            $this->result =  $this->model->limit($length)->offset($offset)->get();





        }







    }


    public function relationQueries() : array
    {
        return [];
    }

    public function relations() :array
    {
        return [];
    }

    public function relationFilters() : array
    {

        return [];

    }

    function initRelations($model)
    {

        $relations = $this->relations();

        foreach ($relations as $relation)
        {

            $model = $model->with($relation);


        }

        return $model;

    }


    public function csvFileColumns()
    {
        return $this->columns();
    }

    public function csvDataBaseColumns()
    {
        return $this->columns();
    }

    public function getCsvResult($result,$withOptions=false)
    {
        return $this->getResult($result,$withOptions);
    }

    public function giveCSV()
    {


        $model = $this->model();
        $items = $model::select($this->csvDataBaseColumns());
        $items = $this->initRelations($items);
        $items = $items->get();



        $items = $this->getCsvResult($items,false);



        $csv_data= array_merge([$this->csvFileColumns()],$items);







        return new StreamedResponse(
            function () use ($csv_data) {
                // A resource pointer to the output stream for writing the CSV to
                $handle = fopen('php://output', 'w');
                foreach ($csv_data as $row) {
                    // Loop through the data and write each entry as a new row in the csv
                    fputcsv($handle, $row);
                }

                fclose($handle);
            },
            200,
            [
                'Content-type'        => 'text/csv',
                'Content-Disposition' => 'attachment; filename=members.csv'
            ]
        );





    }


    //all filter will handle here
    abstract public function filterRegister(Request $request);


    abstract public function model() : string; // Model::class

    abstract public function routePrefix() : string;  //Route::resource("routePrefix") -> Route::get("routePrefix.edit") --- you get the idea

    abstract  public  function extraQuery() : Builder; // for using -> $this->model->with("relation") etc etc  or dunno




    //return your result as array, the result will model of what you are trying to get from $this->model();
    abstract public function getResult($result,$withOptions=true) : array; // return your data as array









    public function totalCount() : int
    {

        return $this->totalCount;

    }

    public function filteredCount(): int
    {
        return $this->filteredCount;
    }


    //if you wanna get relation models, you must add relation columns otherwise it will return null and you cant access em
    abstract  public  function  columns() : array;


    //which columns will be used for dataTable standard search input
    abstract  public function searchColumns() : array;


    //i will use this for index page,
    abstract public function dataTableColumns():array;

    public function getOptions($item,$prop="id",$editParameters=[],$deleteParemeters=[],$viewParameters=[])
    {



        $editRouteParameters["id"]=$item->$prop;
        $deleteRouteParameters["id"]=$item->$prop;




        $editRouteParameters =  array_merge($editRouteParameters,$editParameters);
        $deleteRouteParameters =  array_merge($deleteRouteParameters,$deleteParemeters);



        #### vıew ıcon render

        if($this->isCrud)
        {
            $view = view("DataTable::buttons")->with([

                "editRoute"=>Route("$this->routePrefix.edit",$editRouteParameters),
                "deleteRoute"=>Route("$this->routePrefix.destroy",$deleteRouteParameters),

            ]);





        }
        else
        {
            $viewParameters = array_merge($viewParameters,["id"=>$item->$prop]);
            $view = view("DataTable::viewIcon")->with([

                "viewRoute"=>Route("$this->routePrefix.show",$viewParameters),

            ]);
        }



        return $view->render();





    }

    public function  dataTable()
    {


        $data=[

            "data"=>$this->getResult($this->result),
            "draw"=>$this->request->draw,
            "recordsTotal"=>$this->totalCount(),
            "recordsFiltered"=>$this->filteredCount(),
//            "error"=>"P_T 1 ERROR",




        ];
        $data = $this->utf8ize($data);
        return $data;

    }

    public function utf8ize( $mixed ) {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = $this->utf8ize($value);
            }
        } elseif (is_string($mixed)) {
            return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
        }
        return $mixed;
    }




}