<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 05/04/2018
 * Time: 03:51
 */

namespace Azizyus\DataTableWrapper;


class DataTableBooleanHtml
{


    public static function getTrue($model,$column)
    {

        return "<span data-id='$model->id' data-column='$column' style='cursor: pointer;' class='is-active-changer tag tag-default tag-success'> +++ </span>";

    }

    public static function getFalse($model,$column)
    {

        return "<span data-id='$model->id' data-column='$column' style='cursor:pointer;' class='is-active-changer tag tag-default tag-danger'> --- </span>";

    }

    public static function getActiveColumn($model,$column)
    {

        if($model->$column == true) return DataTableBooleanHtml::getTrue($model,$column) ;
        else return DataTableBooleanHtml::getFalse($model,$column);


    }

}