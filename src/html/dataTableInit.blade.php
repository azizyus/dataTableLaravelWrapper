@php


$isActiveData = ["model"=>$model];
//backward compatibility
if(isset($isActiveExtraData)==false)
{
$isActiveExtraData=[];
}

$isActiveData = array_merge($isActiveData,$isActiveExtraData);



$orderChangeData = ["model"=>$model];


if(isset($orderChangeExtraData)==false)
{
$orderChangeExtraData=[];
}


$orderChangeData = array_merge($orderChangeData,$orderChangeExtraData);




//backward compatibility

@endphp

<!--

<script src="/datatable-lib/draw-table/jquery/jquery.min.js?v=v1.2.3"></script>


-->
<script src="/datatable-lib/draw-table/jquery-ui/js/jquery-ui.min.js?v=v1.2.3"></script>
<link rel="stylesheet" href="/datatable-lib/draw-table/tablednd.css" type="text/css"/>


<script>

    var table;

</script>




<script language = "Javascript">
    $(document).ready(function(){

        $(function() {

            table = $('{{$dataTableSelector}}').DataTable( {

                "order":false,
                "processing": true,
                "serverSide": true,
                "searchable":true,
                "aaSorting" : [[]],
                "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
                "pageLength": 25,
                "ajax": {
                    "type": "GET",
                    "url": "{!! $url !!}"
                },
                'createdRow': function( row, data, dataIndex ) {



                    $(row).attr('id', "id_"+data[0]);
                },
            });

            $('{{$dataTableSelector}}').sortable({
                items: 'tr',
                opacity: 0.6,
                axis: 'y',
                stop: function (event, ui) {

                    var tableInfo  = table.page.info();
                    var currentPage = tableInfo.page;
                    var pageLength = tableInfo.length;

                    var data = $(this).sortable('serialize');

                    console.log(data);

                    $.ajax({
                        data: data,
                        type: 'GET',
                        url: '{!! Route("datatable.onOrderChange",$orderChangeData) !!}&currentPage='+currentPage+'&pageLength='+pageLength
                    });
                }



            });
        });

    });


</script>




<script>



    $(document).on("click",".is-active-changer",function () {


        var id = $(this).attr("data-id");
        var column = $(this).attr("data-column");


        $.ajax({


            "type":"GET",
            "url":"{!! Route("datatable.onIsActiveChanged",$isActiveData)  !!}",
            "data":{column:column,id:id},
            success:function (data) {


              $("[data-id="+id+"][data-column="+column+"]").parent().html(data);

            }

        });



    });


</script>


<link rel="stylesheet" type="text/css" href="/datatable-lib/cdn/css/jquery.dataTables.min.css">
<script src='/datatable-lib/cdn/js/jquery.dataTables.min.js' type="text/javascript"></script>

<script src="/datatable-lib/draw-table/datatables/assets/lib/js/jquery.dataTables.min.js?v=v1.2.3"></script>


<script src="/datatable-lib/draw-table/datatables/assets/lib/extras/TableTools/media/js/TableTools.min.js?v=v1.2.3"></script>
<script src="/datatable-lib/draw-table/datatables/assets/lib/extras/ColVis/media/js/ColVis.min.js?v=v1.2.3"></script>
<script src="/datatable-lib/draw-table/datatables/assets/custom/js/DT_bootstrap.js?v=v1.2.3"></script>
<script src="/datatable-lib/draw-table/datatables/assets/custom/js/datatables.init.js?v=v1.2.3"></script>
<script src="/datatable-lib/draw-table/js/jquery.tablednd.js" type="text/javascript"></script>

