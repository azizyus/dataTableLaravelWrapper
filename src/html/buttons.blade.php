
<i style="font-size: 20px; cursor: pointer;" onclick='window.location="{{$editRoute}}"'  class='fa fa-edit icon'></i>

<i  style="font-size: 20px; cursor: pointer;" class="fa fa-trash custom-delete-button">
    <form class="crud-delete-form" method="POST"  action="{{$deleteRoute}}">
        {{method_field("DELETE")}}
        {{csrf_field()}}

    </form>
</i>