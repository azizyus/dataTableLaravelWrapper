<script>
    $(document).on("click",".custom-delete-button",function (e) {



        swal({
            title: "İşleme Devam Etmek İstediğinize Emin misiniz?",
            text: "(Bu işlem geri alınamaz)",
            icon: "warning",
            buttons: {
                cancel: "İPTAL",
                catch:"DEVAM ET"

            },
            dangerMode: true,
        })
            .then((willDelete) => {
            if (willDelete) {
                $(this).children("form").submit();
            } else {

                swal("İşleminiz İptal Edildi!");
    }
    });
    });
</script>