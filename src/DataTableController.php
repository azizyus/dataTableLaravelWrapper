<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18/03/2018
 * Time: 10:29
 */
namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Azizyus\DataTableWrapper\DataTableBooleanHtml;
use Illuminate\Http\Request;

class DataTableController extends Controller
{

    function restrictedColumns(){


        return [

            "id",
            "created_at",
            "updated_at",
            "deleted_at",
            "order"

        ];

    }

    public function test(Request $request)
    {


        $test =[

            "middleware" => config("datatable.middleware")

        ];



        print_r($test);
        echo "<br>";
        return "SEEMS ALL OK";

    }

    public function orderChange(Request $request)
    {




        $newIdOrders = $request->id;






        $currentPage = $request->currentPage;
        $currentPageLength = $request->pageLength;

        $model = $request->model;




        $models = $model::orderBy("sort","ASC")->offset($currentPage*$currentPageLength)->limit($currentPageLength)->get();




//        return $models;


        //return $newIdOrders;

        $i=$currentPage*$currentPageLength;






        foreach ($models as $key => $modelFromDb)
        {





            $xModel = $models->where("id",$newIdOrders[$key])->first();


            $xModel->sort = $i;
            $xModel->save();

            $i++;

        }




        return "true";




    }



    public function isActiveChange(Request $request)
    {





        $id = $request->id;
        $model = $request->model;

        $column = $request->column;

        if(in_array($column,$this->restrictedColumns()))  return "false";





        $model = $model::where("id",$id)->first();

        $isActive = $model->$column;

        $model->$column = !$isActive;


        $model->save();



        if(!$isActive) return DataTableBooleanHtml::getTrue($model,$column);
        else return DataTableBooleanHtml::getFalse($model,$column);


    }


}